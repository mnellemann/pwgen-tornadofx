# Random Password Generator

This is a simple JavaFX desktop application you can use to generate secure random passwords.

![PasswordFX Screenshot](https://bitbucket.org/mnellemann/passwordfx/downloads/PasswordFx.gif)

## Download

You can download native packaged versions for Windows, Mac OS and Linux (deb) at https://bitbucket.org/mnellemann/passwordfx/downloads/ .

## Build & Test

Clone the repository and use gradle to build, test and run locally.

    ./gradlew build
    ./gradlew run


## Bintray Deployment

    ./gradlew build bintrayUpload

## FxLauncher Deployment


### Upload Artifact

This will upload the artifacts to the ````fxlauncher { deployTarget = '' }```` defined in build.gradle.

    ./gradlew build deployApp


### Build Native Installers

This will build native installers for Windows, Mac and Linux. You need to run this on each target platform and have the required external tools installed. 

    ./gradlew build generateNativeInstaller


#### Linux

* Java JDK 8 with JavaFX (Oracle, Azul ZuluFX or Liberica Full)
* Requirements for Debian Family: `fakeroot dpkg-dev`
* Requirements for RedHat Family: `rpmbuild`


#### Windows

* Java JDK 8 with JavaFX (Oracle, Azul ZuluFX or Liberica Full)
* Install InnoSetup or WiX Toolset (requires .Net 3.5)


#### Mac OS

* Java JDK 8 with JavaFX (Oracle, Azul ZuluFX or Liberica Full)
* Xcode (or git?)
    
    
    export PATH="$(/usr/libexec/java_home)/bin:$PATH"


## License Information

* The application is [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) licensed and free to use in any way.
* Application icon by [Freepik, Flaticon](https://www.freepik.com/) is licensed by Creative Commons BY 3.0.