package biz.nellemann.passwordfx.controller

import tornadofx.*

class MainController: Controller() {

    fun copy(value: String) {
        clipboard.setContent {
            putString(value)
        }
    }

}