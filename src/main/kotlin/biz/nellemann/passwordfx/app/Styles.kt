package biz.nellemann.passwordfx.app

import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.px

class Styles : Stylesheet() {
    companion object {
        val heading by cssclass()
        val wrapper by cssclass()
    }

    init {
        wrapper {
            backgroundColor += Color.WHITE
        }
        label and heading {
            fontSize = 16.px
            fontWeight = FontWeight.BOLD
        }
    }
}