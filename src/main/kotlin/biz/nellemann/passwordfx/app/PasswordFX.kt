package biz.nellemann.passwordfx.app

import biz.nellemann.passwordfx.view.MainView
import tornadofx.*

class PasswordFX:App(MainView::class, Styles::class)