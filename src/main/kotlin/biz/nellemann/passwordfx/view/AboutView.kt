package biz.nellemann.passwordfx.view

import javafx.scene.layout.VBox
import tornadofx.*

class AboutView : View("About PasswordFX") {

    override val root: VBox by fxml("/views/AboutView.fxml")

    fun onClose() {
        close()
    }


}
