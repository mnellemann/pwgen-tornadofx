package biz.nellemann.passwordfx.view

import biz.nellemann.passwordfx.controller.MainController
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.image.Image
import tornadofx.*
import java.util.*
import biz.nellemann.libpw.PasswordGenerator
import javafx.scene.control.*
import javafx.scene.layout.BorderPane


class MainView : View("PasswordFX") {

    val controller: MainController by inject()

    override val root: BorderPane by fxml("/views/MainView.fxml")

    val lengthSlider: Slider by fxid()
    val lengthSpinner: Spinner<Number> by fxid()
    val length = SimpleIntegerProperty(16)

    val checkCapitalized: CheckBox by fxid()
    val checkNumerals: CheckBox by fxid()
    val checkSymbols: CheckBox by fxid()

    val isCapitalized = SimpleBooleanProperty(this, "", config.boolean("capitalized") ?: true)
    val isNumerals = SimpleBooleanProperty(true)
    val isSymbols = SimpleBooleanProperty(false)

    val passwordField: TextField by fxid()
    val password = SimpleStringProperty()

    val pwgen = PasswordGenerator()

    init {
        FX.primaryStage.icons += Image("PasswordFx.png")
        FX.locale = Locale.ENGLISH

        lengthSpinner.bind(length)
        lengthSlider.bind(length)
        passwordField.bind(password)

        checkCapitalized.bind(isCapitalized)
        checkNumerals.bind(isNumerals)
        checkSymbols.bind(isSymbols)

    }

    fun generate() {
        try {
            password.value = pwgen.random(length.value, isCapitalized.value, isNumerals.value, isSymbols.value)
        } catch (e: Exception) {
            System.err.println(e.message)
        }
    }

    fun copy() {
        if(password.value != null) {
            controller.copy(password.value)
        }
    }

    fun menuQuit() {
        FX.primaryStage.close()

    }

    fun menuCopy() {
        if(password.value != null) {
            controller.copy(password.value)
        }
    }

    fun menuAbout() {
        find<AboutView>().openModal()
    }

}